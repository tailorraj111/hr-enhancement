from __future__ import unicode_literals
import frappe
from frappe import msgprint,throw, _
import json

def validate(self,method):
    # frappe.msgprint("in validate")
    if not self.custom_calculation:
        default_doc = frappe.get_single("HR Enhancement Setting")
        # frappe.throw(str(default_doc['wcf']))
        basic,overt,absent,holiday,bonus = 0,0,0,0,0
        for item in self.earnings:
            if item.salary_component == default_doc.basic:
                basic = item.amount
            if item.salary_component == default_doc.overtime_component:
                overt = item.amount
                item.do_not_include_in_total = 1
            if item.salary_component == default_doc.holiday_component:
                holiday = item.amount
                item.do_not_include_in_total = 1
            if item.salary_component == default_doc.bonus_component:
                bonus = item.amount
                item.do_not_include_in_total = 1

        for item in self.deductions:
            if item.salary_component == default_doc.leave_component:
                absent = item.amount
                item.do_not_include_in_total = 1
            if item.salary_component == default_doc.wcf_component:
                item.do_not_include_in_total = 1

        gross_amount = basic + overt + holiday + bonus - absent
        add_component_to_ss(self,"earnings",default_doc.gross_component,gross_amount)

        for key,value in get_deduction_component(self,gross_amount,basic).items():
            add_component_to_ss(self,"deductions",default_doc.get(key),value)
            
        # frappe.throw("stop")

        

        # taxable_income = gross_amount * 0.90
        # nssf = 0.1 *   gross_amount
        # helsb = 0.15 * gross_amount
        # trade_union = 0.1 * basic
        # payee = calculate_payee(basic,taxable_income)
        
        self.calculate_net_pay()
        self.custom_calculation = 1

def add_component_to_ss(self,component_type,component,amount,do_not_include_in_total = 0):
    row = self.append(component_type, {})
    row.salary_component = component
    row.amount = amount
    row.do_not_include_in_total = do_not_include_in_total


def get_deduction_component(self,gross_amount,basic):
    ded_obj = {}
    nssf_component,helsb_component,trade_union,payee_component,wcf = frappe.db.get_value("Employee",self.employee,["nssf_deduction","helsb_deduction","trade_union_deduction","payee_deduction","wcf"])
    if nssf_component :
        ded_obj["nssf_component"] = 0.1 * gross_amount
    if helsb_component:
        ded_obj["helsb_component"] = 0.15 * gross_amount
    if trade_union:
        ded_obj["trade_union"] = 0.01 * basic
    if payee_component:
        ded_obj["payee_component"] = calculate_payee(basic,gross_amount * 0.90)
    if wcf:
        ded_obj["wcf_component"] = 0.01 * gross_amount
        
    
    # frappe.throw(str(ded_obj))
    return ded_obj
    # return{
    # "nssf_component" : 0.1 * gross_amount if nssf_component else 0,
    # "helsb_component" : 0.15 * gross_amount if helsb_component else 0,
    # "trade_union" : 0.01 * basic if trade_union else 0,
    # "payee_component" : calculate_payee(basic,gross_amount * 0.90) if payee_component else 0
    # }
    

def calculate_payee(basic,tax_inc):
    if tax_inc > 1000000:
        return ((tax_inc - 1000000)*0.30)+130500
    elif tax_inc > 760000 and tax_inc <= 1000000:
        return ((tax_inc - 760000)*0.25)+70500
    elif tax_inc > 520000 and tax_inc <= 760000:
        return ((tax_inc-520000)*0.20)+22500
    elif tax_inc > 270000 and tax_inc <= 520000:
        return ((tax_inc-270000)*0.09)
    elif tax_inc <= 270000:
        return 0